<?php

/**
 * 配置文件
 */
return array(
    'DB_TYPE' => 'mysql',
    'DB_HOST' => '#DB_HOST#',
    'DB_NAME' => '#DB_NAME#',
    'DB_USER' => '#DB_USER#',
    'DB_PWD' => 'mysecret',
    'DB_PORT' => '3306',
    'DB_PREFIX' => '#DB_PREFIX#',
    //密钥
    "AUTHCODE" => '#AUTHCODE#',
    //cookies
    "COOKIE_PREFIX" => '#COOKIE_PREFIX#',
);
?>