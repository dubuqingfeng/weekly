#curl

## GET 请求

curl baidu.com

## POST/PUT/DLETE 请求

**-X**

curl baidu **-X POST/PUT/DLETE**

## 在 POST/PUT/DLETE 中传递JSON数据到服务器

**-d**

curl baidu -X POST **-d '{"name":"a","pass":"123"}'**

## 传送cookie到服务器

**-b**

curl baidu -X POST -d '{"name":"a","pass":"123"}' -b **'key=value'**

## 传送Header到服务器

**-H**

curl baidu -X POST -d '{"name":"a","pass":"123"}' -b 'key=value' -H **'Content-type:application/json'**


