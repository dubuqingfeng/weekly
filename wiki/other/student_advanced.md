#  [/entity_user] 

##  获取指定字段 

参数 | 说明
---| ---
fields |  字段名


- request 


		http://drupal/daxuebao/api/entity_user/208?fields=field_avatar
		
- response 200

		{
			  "field_avatar": {
			    "und": [
			      {
			        "fid": "255",
			        "uid": "208",
			        "filename": "file51CrAT",
			        "uri": "public://pictures/user/avatar//file51CrAT",
			        "filemime": "application/octet-stream",
			        "filesize": "4458",
			        "status": "1",
			        "timestamp": "1395118970",
			        "rdf_mapping": [],
			        "full_url": "http://drupal/daxuebao/sites/default/files/pictures/user/avatar//file51CrAT",
			        "alt": "",
			        "title": "",
			        "width": "20",
			        "height": "20"
			      }
			    ]
			  }
			}