# 发表评论 [/comment] [POST]

参数 | 说明
---|---
nid | 对应的nid
comment_body | 评论内容

- request 

		{
		  "nid": "1330",
		  "comment_body": {
		    "und": [
		      {
		        "value": "Comment body text"
		      }
		    ]
		  }
		}
		
- response 200


##  获取评论总数 [/node/{nid}] [GET]

- request 

		http://drupal/daxuebao/api/node/1330
		
- respone 200
		
		...
		"comment_count": "2",
		...
		
##  获取评论列表  [/views/comment_list] [GET]

参数 | 说明
---| ---
args[0] | 对应的Nid

- request 

		http://drupal/daxuebao/api/views/comment_list?args[0]=1330
		
- response 200

		[
		  {
		    "uid": "1",
		    "comment": "http://drupal/daxuebao",
		    "nickname": "13",
		    "avatar" :'http://drupal/daxuebao/sites/default/files/pictures/user/avatar//file51CrAT'
		    “time”:"2014-4-10"
		  }
		]
		
