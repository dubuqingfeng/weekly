[TOC]

**Host: http://ndweekly.sinapp.com/api**

# API document

## ① 文章

-   [文章列表][]

-   [文章详情][]

    -   [文章评论][]

## ② 图片

-   [图片列表][]

## ③ 推荐

-   [乐][]

-   [影][]

-   [游][]

-   [物][]

## ④ 言

-   [创建问答][]

## ⑤ 投稿

## ⑥ 社交

-   [用户登录][]

-   [用户注册][]

-   [我的评论][]

## 接口调整

-   [node/{nid}][]

## 其他（测试与错误码）

-   [curl 命令][]

-   [HTTP返回错误状态码][]

  [文章列表]: 1/article_list
  [文章详情]: 1/article_desc
  [文章评论]: 1/article_comments_list
  [图片列表]: 2/address
  [创建问答]: 4/collect
  [乐]: 3/school_view
  [影]: 3/jobs
  [游]: 3/workflow_management
  [物]: 3/almanac
  [我的评论]: 4/restuarant
  [用户登录]: 6/party
  [用户注册]: 6/group
  [node/{nid}]: issue/node
  [curl 命令]: other/error_code
  [HTTP返回错误状态码]: other/curl