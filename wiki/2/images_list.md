## 图片列表 [/views/get_images_list] [GET]

- 请求地址

http://ndweekly.sinaapp.com/index.php?g=mobile&m=image&a=allimage

BASE_URL + "index.php?g=mobile&m=image&a=allImage";

- 支持格式

JSON

- Header参数

```
{
  "Content-Type":"application/json"
}
```

- 示例

```
	curl -l http://ndweekly.sinaapp.com/index.php?g=mobile&m=image&a=allimage
```

- 请求参数


参数 | 说明
--- | ---
cid | 分类ID 

- request 

		http://ndweekly.sinaapp.com/index.php?g=mobile&m=image&a=allimage
		
 - respone 200
 
 ```
[
{
"slide_id": "1", 
"slide_cid": "0", 
"slide_name": "20150928", 
"slide_pic": "http://ndweekly-data.stor.sinaapp.com/upload/560892aea6bdb.jpg", 
"slide_url": "", 
"slide_des": "", 
"slide_content": "", 
"slide_status": "1", 
"listorder": "0"
}, 
{
"slide_id": "2", 
"slide_cid": "0", 
"slide_name": "2015092801", 
"slide_pic": "http://ndweekly-data.stor.sinaapp.com/upload/560892ebc83a0.jpg", 
"slide_url": "", 
"slide_des": "", 
"slide_content": "", 
"slide_status": "1", 
"listorder": "0"
}
]
```

- 正常返回参数

返回值 | 说明
--- | ---
--- | ---

- 异常返回参数
