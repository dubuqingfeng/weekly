## 获取评论列表 [/views/acticle_comments_list] [GET]

- 请求地址

http://ndweekly.sinaapp.com/index.php?g=mobile&m=article

BASE_URL + "index.php?g=mobile&m=article";

- 支持格式

JSON

- Header参数

```
{
  "Content-Type":"application/json"
}
```

- 示例

```
	curl -l http://ndweekly.sinaapp.com/index.php?g=mobile&m=article&id=1
```

- 请求参数


参数 | 说明
--- | ---
id | 文章ID

- request 

		http://ndweekly.sinaapp.com/index.php?g=mobile&m=article&id=1
		
 - respone 200
 
 ```
 {
    "posts": [
        {
        	"tid": "2", 
            "object_id": "2", 
            "term_id": "1", 
            "listorder": "0", 
            "status": "1", 
            "id": "1", 
            "post_author": "1", 
            "post_keywords": "", 
            "post_source": "", 
            "post_date": "2015-09-27 11:59:51", 
            "post_content":"",
            "post_title": "阿里2015校招面试回忆（成功拿到offer）", 
            "post_excerpt": "继上次《百度2015校园招聘面试题回忆（成功拿到offer）》文章过后，大家都希望除了题目之外，最好能给出自己当时的回答情况，看看有没有什么回答技巧，这样更有参考价值。
嗯，建议的很对，因此这次对于阿里的面试回忆，我下面以对话的形式尽可能复现我当初的面试场景。", 
            "post_status": "1", 
            "comment_status": "1", 
            "post_modified": "2015-09-27 11:58:35", 
            "post_content_filtered": null, 
            "post_parent": "0", 
            "post_type": null, 
            "post_mime_type": "", 
            "comment_count": "0", 
            "smeta": "{\"thumb\":\"\"}", 
            "post_hits": "0", 
            "post_like": "0", 
            "istop": "0", 
            "recommended": "0", 
            "user_login": "admin", 
            "user_pass": "c535018ee94621232f297a57a5a743894a0e4a801fc389af", 
            "user_nicename": "admin", 
            "user_email": "1135326346@qq.com", 
            "user_url": "", 
            "avatar": null, 
            "sex": "0", 
            "birthday": null, 
            "signature": null, 
            "last_login_ip": "118.74.185.104", 
            "last_login_time": "2015-09-27 11:54:15", 
            "create_time": "2015-09-27 11:37:06", 
            "user_activation_key": "", 
            "user_status": "1", 
            "score": "0", 
            "user_type": "1"
        }, 
        {}
    ], 
    "page": null, 
    "count": "2"
}
```

- 正常返回参数

返回值 | 说明
--- | ---
--- | ---

- 异常返回参数
