## 关于 *[node/{nid}] [GET]* 接口

### 其中的图像字段

			"field_image_single": {
			    "und": [
			      {
			        "fid": "468",
			        "uid": "1",
			        "filename": "druplicon.small_.png",
			        "uri": "public://pictures/supermarket/druplicon.small__0.png",
			        "filemime": "image/png",
			        "filesize": "4458",
			        "status": "1",
			        "timestamp": "1396320590",
			        "rdf_mapping": [],
			        "full_url": "http://drupal/daxuebao/sites/default/files/sites/default/pictures/supermarket/druplicon.small__0.png",
			        "alt": "",
			        "title": "",
			        "width": "20",
			        "height": "20"
			      }
			    ]
			  }	
			  
其中的```full_path```不会根据当前域名/ip来自动生成，请android和ios开发者使用```uri```字段，手动进行字符拼接。如:

```"public://pictures/supermarket/druplicon.small__0.png"```

to 

```http://210.73.222.16/daxuebao/sites/default/files/pictures/supermarket/druplicon.small__0.png"```

即把```public://pictures/```替换成```http://210.73.222.16/daxuebao/sites/default/files```